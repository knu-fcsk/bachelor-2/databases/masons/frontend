export function removeMason(state, id) {
  removeItemWithId(state.masons, id)
}

export function removeLodge(state, id) {
  removeItemWithId(state.lodges, id)
}

export function removeEvent(state, id) {
  removeItemWithId(state.events, id)
}

export function clearMasons(state) {
  state.masons = []
}

export function clearLodges(state) {
  state.lodges = []
}

export function clearEvents(state) {
  state.events = []
}

export function addMasons(state, masons) {
  state.masons = state.masons.concat(masons)
}

export function addLodges(state, lodges) {
  state.lodges = state.lodges.concat(lodges)
}

export function addEvents(state, events) {
  state.events = state.events.concat(events)
}

function removeItemWithId(items, id) {
  const index = items.findIndex((el) => el.id == id)
  items.splice(index, 1)
}