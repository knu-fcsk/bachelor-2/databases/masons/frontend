import { name } from "store/storages/cookieStorage";

export default {
  masons: [
    // {
    //   id: 2,
    //   acceptance_data: ,
    //   lodge_id: 1,
    //   name: "John Smith",
    //   profession: "Lawyer",
    //   rank: "Master Mason"
    // }
  ],
  lodges: [
    // {
    //   id:
    //   country:
    //   grand_master:
    //   name:
    // }
  ],
  events: [
    {
      id: 1,
      name: "Event 1",
      date: "11/11/2022",
      description: "Some event",
      lodge_id: 1,
    },
    {
      id: 2,
      name: "Event 2",
      date: "11/11/2022",
      description: "Some event",
      lodge_id: 1,
    },
    {
      id: 3,
      name: "Event 3",
      date: "11/11/2022",
      description: "Some event",
      lodge_id: 2,
    },
    {
      id: 4,
      name: "Event 4",
      date: "11/11/2022",
      description: "Some event",
      lodge_id: 1,
    },
    {
      id: 5,
      name: "Event 5",
      date: "11/11/2022",
      description: "Some event",
      lodge_id: 3,
    }
  ]
}