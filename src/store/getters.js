export function getMasons(state) {
  return state.masons
}

export function getMason(state) {
  return (id) => {
    return state.masons.find(el => el.id == id)
  }
}

export function getLodges(state) {
  return state.lodges
}

export function getLodge(state) {
  return (id) => {
    return state.lodges.find(el => el.id == id)
  }
}

export function getEvents(state) {
  return state.events
}

export function getEvent(state) {
  return (id) => {
    return state.events.find(el => el.id == id)
  }
}
