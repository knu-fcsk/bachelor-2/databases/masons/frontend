
export function emptyValidator(input)
{
  if(!input)
  {
    return "Input cannot be empty"
  }
}

export function passwordValidator(input)
{
  if(input.length < 8)
  {
    return "Password must be 8 symbols or more"
  }
}

export function emailValidator(input)
{
  const email = String(input)
  if(!/(.+)@(.+){2,}\.(.+){2,}/.test(email))
  {
    return "Plese enter valid email"
  }
}

export function nameValidator(input)
{
  if(input.length < 4)
  {
    return "Name must be 4 signs or more"
  }
  else if(input.indexOf(' ') < 0)
  {
    return "Name must contain 2 or more words"
  }
}

export function dateValidator(input)
{
  if(!Date.parse(input))
  {
    return "Enter correct date"
  }
}

export function positiveNumberValidator(input)
{
  const num = Number(input)
  if(!num)
  {
    return "Please enter correct number"
  }
  if(num < 0)
  {
    return "Please enter positive number"
  }
}

export function createSamePasswordValidator(first_password)
{
  return (password) => {
    if(password != first_password)
      return "Passwords are different"
  }
}

export function letterNamesValidator(input)
{
  if( !/^[A-Za-z\s]+$/.test(input) )
  {
    return "Enter correct coutry name"
  }
}