import store from '@/store'

export function convertTimestampToStr(timestamp)
{
  if(timestamp == 0) {
    return "----/--/-- --:--:--"
  }
  const date = new Date(timestamp * 1000)
  const year = date.getFullYear()
  const month = '0' + date.getMonth()
  const day = '0' + date.getDate()

  const result_str = year + '/' + month.substr(-2) + '/' + day.substr(-2)
  return result_str
}

export function convertStrToTimestamp(str)
{
  const milliseconds = Date.parse(str)
  return milliseconds/1000 + 3600*24
}

export function get_mason(data)
{
  let mason = {};
  Object.assign(mason, data.attributes)
  mason.acceptance_date = convertTimestampToStr(mason.acceptance_date)
  mason.id = data.id
  return mason;
}

export function add_masons(response)
{
  let masons = [];
  for(let mason_data of response.data.data)
  {
    masons.push(get_mason(mason_data))
  }
  store.commit("addMasons", masons)
}

export function get_lodge(data)
{
  let lodge = {};
  Object.assign(lodge, data.attributes)
  lodge.id = data.id
  return lodge
}

export function add_lodges(response)
{
  let lodges = [];
  for(let lodge_data of response.data.data)
  {
    lodges.push(get_lodge(lodge_data))
  }
  store.commit("addLodges", lodges)
}

export function get_event(data)
{
  let event = {};
  Object.assign(event, data.attributes)
  event.date = convertTimestampToStr(event.date)
  event.id = data.id
  return event
}

export function add_events(response)
{
  let events = [];
  for(let event_data of response.data.data)
  {
    events.push(get_event(event_data))
  }
  store.commit("addEvents", events)
}