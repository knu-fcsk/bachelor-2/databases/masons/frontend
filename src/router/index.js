import { createRouter, createWebHistory } from 'vue-router'
import MainMenu from '@/views/MainMenu.vue'
import MainLayout from '@/views/MainLayout.vue'
import AllMasons from '@/views/AllMasons.vue'
import EditMason from '@/views/EditMason.vue'
import AllLodges from '@/views/AllLodges.vue'
import EditLodge from '@/views/EditLodge.vue'
import AllEvents from '@/views/AllEvents.vue'
import EditEvent from '@/views/EditEvent.vue'
import EventMasons from '@/views/EventMasons.vue'
import Queries from '@/views/Queries.vue'
import SimpleQueries from '@/views/SimpleQueries.vue'
import ComplexQueries from '@/views/ComplexQueries.vue'
import LodgeMasons from '@/views/LodgeMasons.vue'
import MasonEvents from '@/views/MasonEvents.vue'

import SimpleQuery1 from '@/views/queries/SimpleQuery1.vue'
import SimpleQuery2 from '@/views/queries/SimpleQuery2.vue'
import SimpleQuery3 from '@/views/queries/SimpleQuery3.vue'
import SimpleQuery4 from '@/views/queries/SimpleQuery4.vue'
import SimpleQuery5 from '@/views/queries/SimpleQuery5.vue'

import ComplexQuery1 from '@/views/queries/ComplexQuery1.vue'
import ComplexQuery2 from '@/views/queries/ComplexQuery2.vue'
import ComplexQuery3 from '@/views/queries/ComplexQuery3.vue'

const routes = [
  {
    path: '/',
    name: 'MainLayout',
    component: MainLayout,
    children: [
      {
        path: '/',
        name: 'Menu',
        component: MainMenu
      },
      {
        path: '/masons',
        name: 'AllMasons',
        component: AllMasons
      },
      {
        path: '/mason/:id/events',
        name: 'MasonEvents',
        component: MasonEvents
      },
      {
        path: '/masons/edit/:id',
        name: 'EditMason',
        component: EditMason
      },
      {
        path: '/lodges',
        name: 'AllLodges',
        component: AllLodges
      },
      {
        path: '/lodges/:id/masons',
        name: 'LodgeMasons',
        component: LodgeMasons
      },
      {
        path: '/lodges/edit/:id',
        name: 'EditLodge',
        component: EditLodge
      },
      {
        path: '/events',
        name: 'AllEvents',
        component: AllEvents
      },
      {
        path: '/events/edit/:id',
        name: 'EditEvent',
        component: EditEvent
      },
      {
        path: '/events/:id/masons',
        name: 'EventMasons',
        component: EventMasons
      },
      {
        path: '/queries',
        name: 'Queries',
        component: Queries
      },
      {
        path: '/queries/simple',
        name: 'SimpleQueries',
        component: SimpleQueries,
        children: [
          {
            path: '/queries/simple/1',
            name: 'SimpleQuery1',
            component: SimpleQuery1,
          },
          {
            path: '/queries/simple/2',
            name: 'SimpleQuery2',
            component: SimpleQuery2,
          },
          {
            path: '/queries/simple/3',
            name: 'SimpleQuery3',
            component: SimpleQuery3,
          },
          {
            path: '/queries/simple/4',
            name: 'SimpleQuery4',
            component: SimpleQuery4,
          },
          {
            path: '/queries/simple/5',
            name: 'SimpleQuery5',
            component: SimpleQuery5,
          },
        ]
      },
      {
        path: '/queries/complex',
        name: 'ComplexQueries',
        component: ComplexQueries,
        children: [
          {
            path: '/queries/complex/1',
            name: 'ComplexQuery1',
            component: ComplexQuery1
          },
          {
            path: '/queries/complex/2',
            name: 'ComplexQuery2',
            component: ComplexQuery2
          },
          {
            path: '/queries/complex/3',
            name: 'ComplexQuery3',
            component: ComplexQuery3
          },
        ]
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router;